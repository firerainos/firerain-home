package main

import (
	"github.com/go-flutter-desktop/go-flutter"
)

var options = []flutter.Option{
	flutter.WindowDimensionLimits(1200, 800, 1200, 800),
}
